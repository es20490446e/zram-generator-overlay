#! /bin/bash
set -e

here="$(realpath "$(dirname "${0}")")"
assets="${here}/assets"
build="${here}/_BUILD"
root="${here}/_ROOT"

getSource="${build}/1-getSource"
compile="${build}/2-compile"
makeInstall="${build}/3-makeInstall"

if [[ -z "${MAKEFLAGS}" ]]; then
	MAKEFLAGS="--jobs=$(nproc)"
	export MAKEFLAGS
fi


mainFunction () {
	if [[ ! -d "${root}" ]]; then
		getSource
		compile
		makeInstall
		makeRoot
	fi
}


checkDependencies () {
	local missing=()

	local dependencyLists=(
		"${here}/info/dependencies.txt"
		"${here}/info/dependencies/building.txt"
		"${here}/info/dependencies/installing.txt"
	)

	for dependencyList in "${dependencyLists[@]}"; do
		if [[ -f "${dependencyList}" ]]; then
			local lines; readarray -t lines < <(awk 'NF' "${dependencyList}")

			for line in "${lines[@]}"; do
				local name; name="$(echo "${line}" | cut --delimiter='"' --fields=2)"
				local path; path="$(echo "${line}" | cut --delimiter='"' --fields=4)"
				local web; web="$(echo "${line}" | cut --delimiter='"' --fields=6)"

				if [[ -n "${web}" ]]; then
					local web="(${web})"
				fi

				if [[ ! -f "${path}" ]]; then
					missing+=("${name}  ${web}")
				fi
			done
		fi
	done

	if [[ "${#missing[@]}" -gt 0 ]]; then
		echo "Missing required software:" >&2
		echo >&2
		printf '%s\n' "${missing[@]}" >&2
		echo >&2
		exit 1
	fi
}


compile () {
	if [[ ! -d "${compile}" ]]; then
		so cp --archive "${getSource}" "${compile}..."
		cd "${compile}..."
		so make CARGOFLAGS="--target-dir=target" build man
		so make CARGOFLAGS="--target-dir=target" check
		so mv "${compile}..." "${compile}"
	fi
}


getSource () {
	if [[ ! -d "${getSource}" ]]; then
		so git clone \
			"https://github.com/systemd/zram-generator" \
			--single-branch \
			--branch "v$(version generator)"\
			--depth 1 \
			--shallow-submodules \
			"${getSource}..."
		so mv "${getSource}..." "${getSource}"
	fi
}


libDir () {
	cd "${makeInstall}/root"

	find . -name "*.service" |
	head -n1 |
	cut --delimiter='/' --fields=2- |
	rev |
	cut --delimiter='/' --fields=4- |
	rev
}


makeInstall () {
	if [[ ! -d "${makeInstall}" ]]; then
		so cp --archive "${compile}" "${makeInstall}..."
		cd "${makeInstall}..."

		so make CARGOFLAGS="--target-dir=target" DESTDIR="root" install
		so install -Dm644 "LICENSE" -t "root/usr/share/licenses/zram-generator"

		so mv "${makeInstall}..." "${makeInstall}"
	fi
}


makeRoot () {
	local libDir; libDir="$(libDir)"

	so cp --archive "${makeInstall}/root" "${root}..."
	cd "${root}..."

	so install -Dm644 "${assets}/systemd-zram-setup@.service" -t "${root}.../${libDir}/systemd/system"
	so install -Dm644 "${assets}/zram-generator.conf" -t "${root}.../etc/systemd"
	so install -Dm755 "${assets}/zram-setup" -t "${root}.../${libDir}/systemd/system-generators"
	so install -Dm644 "${assets}/zram-setup.8" -t "${root}.../usr/share/man/man8"

	so mv "${root}..." "${root}"
}


prepareEnvironment () {
	renice 10 "${$}" >/dev/null
	checkDependencies
	removeIncompleteDirs "${here}"
	removeIncompleteDirs "${build}"
}


removeIncompleteDirs () {
	local dir="${1}"

	if [[ -d "${dir}" ]]; then
		find "${dir}" \
			-mindepth 1 \
			-maxdepth 1 \
			-type d \
			-name '*...' \
			-print0 |
		xargs -0 rm --recursive --force
	fi
}


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


version () {
	local option="${1}"
	"${here}/version.sh" "${option}"
}


prepareEnvironment
mainFunction
