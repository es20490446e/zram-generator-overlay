#! /bin/bash
set -e


mainFunction () {
	local option="${*}"

	if [[ -z "${option}" ]]; then
		echo "$(generatorVersion).$(smartVersion)"
	elif [[ "${option}" == "generator" ]]; then
		generatorVersion
	elif [[ "${option}" == "smart" ]]; then
		smartVersion
	else
		echo "invalid option: ${option}" >&2
		echo "valids are: empty generator smart" >&2
		exit 1
	fi
}


checkRepoInfo () {
	if [[ ! -x "/usr/bin/repoInfo" ]]; then
		echo "missing required software: repoInfo" >&2
		echo "website: https://gitlab.com/es20490446e/repoInfo" >&2
		exit 1
	fi
}


generatorVersion () {
	repoInfo tags "https://github.com/systemd/zram-generator" |
	cut --delimiter='v' --fields=2- |
	grep --invert-match "[[:alpha:]]" |
	head -n1
}


smartVersion () {
	repoInfo epoch "https://gitlab.com/es20490446e/zram-generator-smart"
}


checkRepoInfo
mainFunction "${@}"
