This repo builds [zram-generator](https://github.com/systemd/zram-generator) by adding features that prevent performance problems and conflicts:

- Zram is only enabled if there is no other kind of swap.
- If zram is enabled, zswap is disabled automatically.
- Uses [zram-conf](https://gitlab.com/es20490446e/zram-conf) to improve performance.
